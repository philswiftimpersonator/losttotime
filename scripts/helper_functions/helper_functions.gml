

///@function shake_camera(x, y);
function shake_camera(xx, yy) {
	
	if(instance_exists(obj_camera))
	{
		obj_camera.shake[0] = argument[0];
		obj_camera.shake[1] = argument[1];
	}
	
}

///@function set_camera_follow(obj, smooth_follow);
///@arg object
///@arg smooth_follow
function set_camera_follow(_obj, smooth) {
	
	if(instance_exists(obj_camera))
	{
		obj_camera.following = _obj;
		obj_camera.smooth_follow = smooth;
	}
	
}

///@function warp_to_room(_room, _warp_speed, color, array);
///@arg room_to
///@arg speed
///@arg color
///@arg pos_array[]
///@arg face_direction
function warp_to_room(room_to, spd, color, array, _new_face) {
	
	var inst = instance_create_depth(0, 0, -2000, obj_warp);
	
	inst.warp_to = room_to;
	inst.new_pos = array;
	inst.spd = spd;
	inst.fade_color = color;
	inst.new_face = _new_face;
}

///@function approach(current, target, amount);
///@arg current
///@arg target
///@arg amount
function approach(current, target, amount) {
	
	if (current < target) {
		return min(current+amount, target); 
	} else {
	    return max(current-amount, target);
	}
	
}

//I actually have very little idea as to what this code in the next two functions does exactly. I got it a long 
//time ago, and tinkering with it seems to have fixed it with my dynamic resolution system. I'm just trying
//to get it consistant but it's kind of hard to do.

/// @function x_to_gui(x)
/// @param x
function x_to_gui(xx) {
	
	//What is the deal with this formula? I'm not good at math so it's hard to tell.
	var t = application_get_position();
	var new_coord = (argument0 - obj_camera.new_x) / (display_get_gui_width() * obj_display.preset_num) * (t[2] - t[0]);
	
	return new_coord*2; //multiplying by 2 fixes it?
	
}

/// @function y_to_gui(y)
/// @param y
function y_to_gui(yy) {
	
	var t = application_get_position();
	var new_coord = (argument0 - obj_camera.new_y) / (display_get_gui_height() * obj_display.preset_num) * (t[3] - t[1]);
	
	return new_coord*2;

}

/// @function play_music()
/// @param id
/// @param loop
function play_music() {
	
	return audio_play_sound_on(global.mus_emitter, argument[0], argument[1], 10);
	
}

/// @function play_sound()
/// @param id
/// @param loop
function play_sound() {
	
	return audio_play_sound_on(global.sfx_emitter, argument[0], argument[1], 10);
	
}

/// @function draw_nine_slice(sprite, x1, y1, x2, y2);
/// @param sprite
/// @param  x1
/// @param  y1
/// @param  x2
/// @param  y2
/// @param color_outline
/// @param color_body
/// @param alpha
function draw_nine_slice(_sprite, xx, yy, xx2, yy2, outline, body, _alpha) {
	
	//Made by the lovely Heartbeast on Youtube, check out the tutorial I got this from: https://www.youtube.com/watch?v=GkH5CIamSyA
	var sprite = argument0;
	var x1 = argument1;
	var y1 = argument2;
	var x2 = argument3;
	var y2 = argument4;

	var slice_width = sprite_get_width(sprite)/3;
	var slice_height = sprite_get_height(sprite)/3;

	if (sprite_get_width(sprite)%3 != 0 || sprite_get_height(sprite)%3 != 0) {
	show_debug_message("WARNING: Sprite is not a multiple of 3.");
	}

	var width = abs(x2-x1);
	var height = abs(y2-y1);
	var col_outline = argument5;
	var col_body = argument6;
	var alpha = argument7;

	// Top
	draw_sprite_part_ext(sprite, 0, 0, 0, slice_width, slice_height, x1, y1, 1, 1, col_outline, alpha); // Left
	draw_sprite_part_ext(sprite, 0, slice_width, 0, slice_width, slice_height, x1+slice_width, y1, (width-slice_width*2)/slice_width, 1, col_outline, alpha); // Middle
	draw_sprite_part_ext(sprite, 0, slice_width*2, 0, slice_width, slice_height, x2-slice_width, y1, 1, 1, col_outline, alpha); // Right

	// Middle
	draw_sprite_part_ext(sprite, 0, 0, slice_height, slice_width, slice_height, x1, y1+slice_height, 1, (height-slice_height*2)/slice_height, col_outline, alpha); // Left
	draw_sprite_part_ext(sprite, 0, slice_width, slice_height, slice_width, slice_height, x1+slice_width, y1+slice_height, (width-slice_width*2)/slice_width, (height-slice_height*2)/slice_height, col_body, alpha); // Middle
	draw_sprite_part_ext(sprite, 0, slice_width*2, slice_height, slice_width, slice_height, x2-slice_width, y1+slice_height, 1, (height-slice_height*2)/slice_height, col_outline, alpha); // Right

	// Bottom
	draw_sprite_part_ext(sprite, 0, 0, slice_height*2, slice_width, slice_height*2, x1, y2-slice_height, 1, 1, col_outline, alpha); // Left
	draw_sprite_part_ext(sprite, 0, slice_width, slice_height*2, slice_width, slice_height, x1+slice_width, y2-slice_height, (width-slice_width*2)/slice_width, 1, col_outline, alpha); // Middle
	draw_sprite_part_ext(sprite, 0, slice_width*2, slice_height*2, slice_width, slice_height, x2-slice_width, y2-slice_height, 1, 1, col_outline, alpha); // Right
}

///@function reset_dialogue_defaults
function reset_dialogue_defaults() {
	myTextbox			= noone;
	myText				= -1;
	mySpeaker			= -1;
	myEffects			= 0;
	myTextSpeed			= 0;
	myTypes				= 0;
	myNextLine			= 0;
	myScripts			= 0;
	myTextCol			= 0;
	myEmotion			= 0;
	myEmote				= -1;
}

///@function create_textevent
///@arg Text
///@arg Speaker
///@arg *Effects
///@arg *Speed
///@arg *Type
///@arg *Next_Line
///@arg *Scripts
///@arg *Text_Col
///@arg *Emotion
///@arg *Emote
function create_textevent() {
	
	//if(instance_exists(obj_textevent)){ exit; }

	var arg_count = argument_count;
	var i = 0, var arg; repeat(arg_count){
		arg[i] = argument[i];
		i++;
	}

	var textevent = instance_create_layer(0,0,"Instances",obj_textevent);

	with(textevent){
		reset_dialogue_defaults();
	
		switch(arg_count-1){
			case 9: myEmote		= arg[9];
			case 8: myEmotion	= arg[8];
			case 7: myTextCol	= arg[7];
			case 6: myScripts	= arg[6];
			case 5: myNextLine	= arg[5];
			case 4: myTypes		= arg[4];
			case 3: myTextSpeed	= arg[3];
			case 2: myEffects	= arg[2];
		}
		mySpeaker	= arg[1];
		myText		= arg[0];
	
		event_perform(ev_other, ev_user0);
	}

	return textevent;
}

///@function create_dialogue
///@arg Text
///@arg Speaker
///@arg Style
///@arg *Effects
///@arg *Speed
///@arg *Type
///@arg *Next_Line
///@arg *Scripts
///@arg *Text_Col
///@arg *Emotion
///@arg *Emote
function create_dialogue() {
	
	//if(instance_exists(obj_textbox)){ exit; }

	//Create the Textbox
	var _textbox = instance_create_depth(x,y, -250, obj_textbox);

	//Get Arguments
	var arg = 0, i = 0, arg_count = argument_count;
	repeat(arg_count){ arg[i] = argument[i]; i++; } 

	//Get arguments
	var _text = arg[0];
	var _speaker, text_len;

	//If Text or Speaker aren't arrays (single line input), make them arrays 
	if(is_array(_text))		{ text_len = array_length_1d(_text); }
	else					{ text_len = 1; _text[0] = _text;  }
	if(!is_array(arg[1]))	{ _speaker = array_create(text_len, id); }
	else					{ _speaker = arg[1]; }

	//Get rest of arguments, fill with default
	var _effects	= array_create(text_len, [1,0]);
	var _speed		= array_create(text_len, [1,0.5]);
	var _textcol	= array_create(text_len, [1,c_white]);
	var _type		= array_create(text_len, 0);
	var _nextline	= array_create(text_len, 0);
	var _script		= array_create(text_len, 0);
	var _emotion	= array_create(text_len, 0);
	var _emotes		= array_create(text_len, -1);
	var _creator	= array_create(text_len, id);
	var _style = 0;

	var a;
	//Fill variables depending on argument count
	switch(arg_count-1){
		case 10:	a = arg[10]; if(array_length_1d(a) != text_len){ a[text_len] = 0; } for(i = 0; i < text_len; i++){ if(a[i] != 0) _emotes[i] = a[i]; }
		case 9: a = arg[9]; if(array_length_1d(a) != text_len){ a[text_len] = 0; } for(i = 0; i < text_len; i++){ if(a[i] != 0) _emotion[i] = a[i]; }
		case 8: a = arg[8];	if(array_length_1d(a) != text_len){ a[text_len] = 0; } for(i = 0; i < text_len; i++){ if(a[i] != 0) _textcol[i] = a[i]; }
		case 7: a = arg[7];	if(array_length_1d(a) != text_len){ a[text_len] =-1; } for(i = 0; i < text_len; i++){ if(a[i] !=-1) _script[i] = a[i]; }
		case 6: a = arg[6];	if(array_length_1d(a) != text_len){ a[text_len] = 0; } for(i = 0; i < text_len; i++){ if(a[i] != 0) _nextline[i] = a[i]; }
		case 5: a = arg[5];	if(array_length_1d(a) != text_len){ a[text_len] = 0; } for(i = 0; i < text_len; i++){ if(a[i] != 0) _type[i] = a[i]; }
		case 4: a = arg[4];	if(array_length_1d(a) != text_len){ a[text_len] = 0; } for(i = 0; i < text_len; i++){ if(a[i] != 0) _speed[i] = a[i]; }
		case 3: a = arg[3];	if(array_length_1d(a) != text_len){ a[text_len] = 0; } for(i = 0; i < text_len; i++){ if(a[i] != 0) _effects[i] = a[i]; }
		case 2: a = arg[2]; _style = a;
	}

	//Change the Textbox Values
	with(_textbox){
		creator		= _creator;
		effects		= _effects;
		text_speed	= _speed;
		type		= _type;
		text		= _text;
		nextline	= _nextline;
		executeScript = _script;
		text_col	= _textcol;
		emotion		= _emotion;	
		emotes		= _emotes;
		t_type      = _style;
	
		//Speaker's Variables
		i = 0; repeat(text_len){
			portrait[i]			= _speaker[i].myPortrait;
			voice[i]			= _speaker[i].myVoice;
			font[i]				= _speaker[i].myFont;
			name[i]				= _speaker[i].myName;
			speaker[i]			= _speaker[i];
		
			if(variable_instance_exists(_speaker[i], "myPortraitTalk"))		{ portrait_talk[i] = _speaker[i].myPortraitTalk; }
			else { portrait_talk[i] = -1; }
			if(variable_instance_exists(_speaker[i], "myPortraitTalk_x"))	{ portrait_talk_x[i] = _speaker[i].myPortraitTalk_x; }
			else { portrait_talk_x[i] = -1; }
			if(variable_instance_exists(_speaker[i], "myPortraitTalk_y"))	{ portrait_talk_y[i] = _speaker[i].myPortraitTalk_y; }
			else { portrait_talk_y[i] = -1; }
			if(variable_instance_exists(_speaker[i], "myPortraitIdle"))		{ portrait_idle[i] = _speaker[i].myPortraitIdle; }
			else { portrait_idle[i] = -1; }
			if(variable_instance_exists(_speaker[i], "myPortraitIdle_x"))	{ portrait_idle_x[i] = _speaker[i].myPortraitIdle_x; }
			else { portrait_idle_x[i] = -1; }
			if(variable_instance_exists(_speaker[i], "myPortraitIdle_y"))	{ portrait_idle_y[i] = _speaker[i].myPortraitIdle_y; }
			else { portrait_idle_y[i] = -1; }
		
		
			if(portrait_talk[i] != -1){ 
				portrait_talk_n[i] = sprite_get_number(portrait_talk[i]);
				portrait_talk_s[i] = sprite_get_speed(portrait_talk[i])/room_speed;
			}
			if(portrait_idle[i] != -1){ 
				portrait_idle_n[i] = sprite_get_number(portrait_idle[i]);
				portrait_idle_s[i] = sprite_get_speed(portrait_idle[i])/room_speed;
			}
			i++;
		}
	
		draw_set_font(font[0]);
		charSize = string_width("M");
		stringHeight = string_height("M");
		event_perform(ev_alarm, 0);	//makes textbox perform "setup"
	}

	myTextbox = _textbox;
	return _textbox;
}

///@function script_execute_alt(ind, [arg1,arg2,...])
///@arg ind
///@arg [arg1,arg2,...]
function script_execute_alt() {
	
	var s = argument0;
	var a = argument1;
	var len = array_length(argument1);

	switch(len){
		case 0 : return script_execute(s); break;
		case 1 : return script_execute(s, a[0]); break;
		case 2:  return script_execute(s, a[0], a[1]); break;
		case 3:  return script_execute(s, a[0], a[1], a[2]); break;
		case 4:  return script_execute(s, a[0], a[1], a[2], a[3]); break;
		case 5:  return script_execute(s, a[0], a[1], a[2], a[3], a[4]); break;
		case 6:  return script_execute(s, a[0], a[1], a[2], a[3], a[4], a[5]); break;
		case 7:  return script_execute(s, a[0], a[1], a[2], a[3], a[4], a[5], a[6]); break;
		case 8:  return script_execute(s, a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7]); break;
		case 9:  return script_execute(s, a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], a[8]); break;
		case 10: return script_execute(s, a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], a[8], a[9]); break;
		case 11: return script_execute(s, a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], a[8], a[9], a[10]); break;
		case 12: return script_execute(s, a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], a[8], a[9], a[10], a[11]); break;
		case 13: return script_execute(s, a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], a[8], a[9], a[10], a[11], a[12]); break;
		case 14: return script_execute(s, a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], a[8], a[9], a[10], a[11], a[12], a[13]); break;
		case 15: return script_execute(s, a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], a[8], a[9], a[10], a[11], a[12], a[13], a[14]); break;
		case 16: return script_execute(s, a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], a[8], a[9], a[10], a[11], a[12], a[13], a[14], a[15]); break;
	}

}
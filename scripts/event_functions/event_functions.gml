

///@function call_event(event_array)
///@arg event_array[]
function call_event(evnt_arry) {
	
	var inst = instance_create_depth(0, 0, -100, obj_event);
	
	inst.evnts = evnt_arry;
	
	return inst;
}

///@function evnt_scene_wait(seconds)
///@args seconds
function evnt_scene_wait(seconds) {
	
	timer++;
	
	if(timer >= argument[0] * 60)
	{
		evnt_end_action();
	}
}

///@function evnt_end_action();
function evnt_end_action() {
	
	evnt_page++;
	
	if(evnt_page > array_length(evnts) - 1)
	{
		instance_destroy();
	}
}

///@function evnt_textbox(text[], speaker[], etc...);
///@arg text[]
///@arg speaker[]
///@arg *style
///@arg *effects
///@arg *speed
///@arg *type
///@arg *nextLine
///@arg *scripts
///@arg *text_col
function evnt_textbox(txt, speaker) {
	
	if(text == noone)
	{
		var args = [txt, speaker];
		//Only Apply The Arguments If They Exist
		for(var i = 2; i < argument_count; i++)
		{
			args[i] = argument[i];
		}
		
		//Call the Textbox Functions Like Normal With The Right Arguments
		text = script_execute_alt(create_dialogue, args);
	}
	
	if(!instance_exists(text))
	{
		text = noone;
		evnt_end_action();
	}
}

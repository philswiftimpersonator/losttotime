
///@function move_entity()
function move_entity() {
	var _layer = "Collision";
	
	var hsp = hspd[0] + hspd[1];
	var vsp = vspd[0] + vspd[1];
	
	
	/*From Zack Bell Games Tutorial, Edited To Fit With The Precise Tile Movement From YOYO Game's Blog Post: It Was Helpful :D */
	
	//Move Up and Down Slope (X)
	if(tile_meeting_precise(x + sign(hsp), y, _layer))
	{
		for(var i = 1; i < 4; i++)
		{
			if(!tile_meeting_precise(x + sign(hsp), y - i, _layer))
			{
				y -= i;
				//spd_multi = 1 - i / 8;
				
				hspd[0] *= 0.88;
				
				break;
			} else if(!tile_meeting_precise(x + sign(hsp), y + 1, _layer)) { 
				y += i;
				//spd_multi = 1 - i / 8;
				
				hspd[0] *= 0.88;
				
				break;
			}
		}
	}
	
	//Move Away From Slope (Y)
	if(tile_meeting_precise(x, y+sign(vsp), _layer))
	{
		for(var i = 1; i < 4; i++)
		{
			if(!tile_meeting_precise(x - i, y+sign(vsp), _layer))
			{
				x -= i;
				//spd_multi = 1 - i / 8;
				
				vspd[0] *= 0.88;
				
				break;
			} else if(!tile_meeting_precise(x + i, y+sign(vsp), _layer)) {
				x += i;
				//spd_multi = 1 - i / 8;
				
				vspd[0] *= 0.88;
				
				break;
			}
		}
	}
	
	//Recalculate Speed Again
	hsp = hspd[0] + hspd[1];
	vsp = vspd[0] + vspd[1];
	
	/*End of Code From Zack Bell Games*/
	
	
	
	//Check For Horizontal Collisions
	if(tile_meeting_precise(x+hsp, y, _layer) || place_meeting(x+hsp, y, par_solid))
	{
		while(!tile_meeting_precise(x+sign(hsp), y, _layer) && !place_meeting(x+sign(hsp), y, par_solid))
		{
			x += sign(hsp);
		}
	
		hspd[0] = 0;
		hsp = 0;
	}

	x += hsp;

	//Do Vertical Collisions
	if(tile_meeting_precise(x, y+vsp, _layer) || place_meeting(x, y+vsp, par_solid))
	{
		while(!tile_meeting_precise(x, y+sign(vsp), _layer) && !place_meeting(x, y+sign(vsp), par_solid))
		{
			y += sign(vsp);
		}
	
		vspd[0] = 0;
		vsp = 0;
	}

	y += vsp;
	
	x = round(x);
	y = round(y);
	
}

///@function animate_entity(face);
///@args direction
function animate_entity(_direction) {
	
	if(!can_move) exit;
	
	sprite_index = sprite[_direction];
	
	//Start Animating When The Entity First Moves
	if(hspd[0] != 0 || vspd[0] != 0)
	{
		if(animate_speed == 0) 
		{
			if(last_walk == 0) image_index = 2; else if(last_walk == 2) { image_index = 0; }
			
			last_walk = image_index;
		}
		animate_speed = sprite_get_speed(sprite_index);
	}
	
	//If We Aren't Past the Number of Sub-Images We Animate, Otherwise Reset Image Index
	if(image_index < sprite_get_number(sprite_index))
	{
		var multi = 1 + sprinting / 2;
		
		image_index += ((animate_speed * multi) / 60) * move;
	} else if(image_index == sprite_get_number(sprite_index)) {
		image_index = 0;
	}
	
	//Stop Animating and Stay On Standing Frame
	if(hspd[0] == 0 && vspd[0] == 0)
	{
		image_index = 1;
		animate_speed = 0;
	}
	
}



///@function tile_meeting(x, y, layer)
function tile_meeting(xx, yy, _layer) {
	var _tm = layer_tilemap_get_id(_layer);
	
	var _x1 = tilemap_get_cell_x_at_pixel(_tm, bbox_left + (argument[0] - x), y),
		_y1 = tilemap_get_cell_y_at_pixel(_tm, x, bbox_top + (argument[1] - y)),
		_x2 = tilemap_get_cell_x_at_pixel(_tm, bbox_right + (argument[0] - x), y),
		_y2 = tilemap_get_cell_y_at_pixel(_tm, x, bbox_bottom + (argument[1] - y));
		
	for(var _x = _x1; _x <= _x2; _x++)
	{
		for(var _y = _y1; _y <= _y2; _y++)
		{
			if(tile_get_index(tilemap_get(_tm, _x, _y)))
			{
				return true;
			}
		}
	}
	
	return false;
}

///@function tile_meeting_precise(x, y, layer)
function tile_meeting_precise(xx, yy, _layer) {
	var _tm = layer_tilemap_get_id(_layer),
		_checker = obj_precise_tile;
		
	//Create A Precise Tile Checker if it Doesn't Already Exist	
	if(!instance_exists(_checker)) instance_create_depth(0, 0, 0, _checker);
	
	var _x1 = tilemap_get_cell_x_at_pixel(_tm, bbox_left + (argument[0] - x), y),
		_y1 = tilemap_get_cell_y_at_pixel(_tm, x, bbox_top + (argument[1] - y)),
		_x2 = tilemap_get_cell_x_at_pixel(_tm, bbox_right + (argument[0] - x), y),
		_y2 = tilemap_get_cell_y_at_pixel(_tm, x, bbox_bottom + (argument[1] - y));
		
	for(var _x = _x1; _x <= _x2; _x++)
	{
		for(var _y = _y1; _y <= _y2; _y++)
		{
			var _tile = tile_get_index(tilemap_get(_tm, _x, _y));
			
			if(_tile)
			{
				if(_tile == 1) return true;
				
				_checker.x = _x * tilemap_get_tile_width(_tm);
				_checker.y = _y * tilemap_get_tile_height(_tm);
				_checker.image_index = _tile;
				
				if(place_meeting(argument[0], argument[1], _checker))
				{
					return true;
				}
				
			}
		}
	}
	
	return false;
}
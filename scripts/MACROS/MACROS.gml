///@description Macros

#macro TILE_WIDTH 16
#macro TILE_HEIGHT 16

#macro GUI_WIDTH 640
#macro GUI_HEIGHT 360

#macro RIGHT 0
#macro UP 1
#macro LEFT 2
#macro DOWN 3
#macro ANY_FACE -1
/// @description Player Variables

event_inherited();

enum states {
	idle = 0,
	walking
}

image_speed = 0;

state = states.idle;

interact_target = noone;

//Sprite Array
sprite = [spr_playerplace_right, spr_playerplace_up, spr_playerplace_left, spr_playerplace_down];

animate_speed = 0;

//Standard Speed
spd = 1;
spd_multi = 1;

//Movement Variables
sprinting = false;

//Last Walk Frame
last_walk = 0;

hspd[1] = 0;
vspd[1] = 0;

//Direction Variables
face = 0;
dir = 0;

xpos = x div TILE_WIDTH;
ypos = y div TILE_HEIGHT;
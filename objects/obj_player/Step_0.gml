/// @description Move and Animate Entity


var key_right = keyboard_check(vk_right);
var key_left = keyboard_check(vk_left);
var key_up = keyboard_check(vk_up);
var key_down = keyboard_check(vk_down);

var sprint_key = keyboard_check(vk_shift);

move_x = key_right - key_left;
move_y = key_down - key_up;

move = (move_x != 0 || move_y != 0) && (can_move);

if(move != false)
{
	dir = point_direction(0, 0, move_x, move_y);
}


if(sprint_key)
{
	sprinting = true;
	
	spd_multi = lerp(spd_multi, 2, 0.1);
} else {
	sprinting = false;
	
	spd_multi = lerp(spd_multi, 1, 0.1);
}

if(hspd[0] == 0 && vspd[0] == 0)
{
	spd_multi = 1;
}

//Face Direction
face = round(dir / 90);
if(face == 4)
{
	face = 0;
}

//Call Animate Function
animate_entity(face);

hspd[0] = move_x * spd * spd_multi * can_move;
vspd[0] = move_y * spd * spd_multi * can_move;

move_entity();

//Set Grid Position (Useful for Event System Later)
xpos = (x - 1) div 16;
ypos = (y + 4) div 16; //Find Player's Foot Position By Adding 4 to the Y Position

//Decide Who We Can Interact With
if(distance_to_object(par_npc) < 12)
{
	interact_target = instance_nearest(x, y, par_npc);
} else {
	interact_target = noone;
}
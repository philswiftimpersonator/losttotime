/// @description Store Display Properties

//Set Up Display Functions
event_user(0);

//Preset Resolutions
preset = [[640, 360], [1280, 720], [1920, 1080]];
preset_num = 1;
new_res = [640, 360];

var current_res = [display_get_width(), display_get_height()];

for(var i = 1; i <= 3; i++)
{
	//Store The Actual Resolution Data
	var res = preset[i-1];
	
	//Check if User Resolution is Higher Than Target Resolution
	if(current_res[0] > res[0] && current_res[1] > res[1])
	{
		//Set the New Resolution
		new_res = res;
		
		preset_num = i;
	}
}

print(new_res[0], "x", new_res[1]);
print(preset_num);

//Set the New Resolution Based On User Display
set_resolution(new_res[0], new_res[1]);
//set_resolution(640, 360);
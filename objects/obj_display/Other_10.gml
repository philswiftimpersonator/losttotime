/// @description Display Functions

///@function set_resolution
///@args width
///@args height
function set_resolution(width, height) {
	
	
	//Set Window Size
	window_set_size(width, height);
	
	var res = preset[0];
	
	//Set GUI Size
	//display_set_gui_size(640, 360);
	display_set_gui_maximize(preset_num, preset_num);
	
	//Call Alarm to Recenter Window
	alarm[0] = 1;
	
	return true;
}
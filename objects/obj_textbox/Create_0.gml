/// @description Init

/*	Created by FriendlyCosmonaut
	Check Out The Itch.io page for this resource :)
	https://friendlycosmonaut.itch.io/dialoguesystem 
*/

if(instance_number(obj_textevent)>5 or instance_number(obj_textbox)>5){ instance_destroy(); exit; }

//-----------Customise (FOR USER)
interact_key		= ord("Z");
up_key				= vk_up;		//for dialogue choices
down_key			= vk_down;		//for dialogue choices
auto_adv = false;
adv_timer = -1;

//Stop Entities From Moving
with(par_entity)
{
	can_move = false;
}

//Text Type
t_type = 0;
fade = false;
glt_string = "";

scale				= 3;
_shake = 0;
x_buffer			= 12 * scale;
y_buffer			= 9 * scale;

portrait_frame		= spr_portraitframe;
dialogue_box		= spr_dialoguebox;
name_box			= spr_namebox;
finished_effect		= spr_arrow;
emote_sprite		= spr_emotes;

choice_snd_effect	= sfx_menu_blip;
select_snd_effect	= sfx_menu_select;

default_col			= c_black;
choice_col			= c_red;
select_col			= c_yellow;
name_col			= c_navy;

name_font			= fnt_moderndos;

priority_snd_effect = 5;
open_mouth_frame	= 1;	//You only need to change this if you are using animated sprites
							//Set this to equal the frame where the mouth is OPEN for talking sprites

//-----------Setup (LEAVE THIS STUFF)
#region
portrait_talk	= -1;
portrait_talk_n = -1;
portrait_talk_s = -1;
portrait_talk_c = 0;

portrait_idle	= -1;
portrait_idle_n = -1;
portrait_idle_s = -1;
portrait_idle_c = 0;

emotes			= -1;
speaker			= noone;

boxHeight		= (sprite_get_height(dialogue_box) + 8) * scale;
boxWidth		= sprite_get_width(dialogue_box) * scale;
gui_width		= display_get_gui_width();
gui_height		= display_get_gui_height();
gb_diff			= gui_width - boxWidth;
portraitWidth	= sprite_get_width(portrait_frame) * scale;
finishede_num	= sprite_get_number(finished_effect);
finishede_spd	= (15/room_speed);

pos_x			= 133;
pos_y			= 180;

name_box_x		= pos_x + (8 * scale);
name_box_y		= pos_y - (23 * scale);
name_box_text_x = x_to_gui(pos_x - 158);
name_box_text_y = y_to_gui(pos_y - 48);

finishede_x		= (pos_x + boxWidth - x_buffer) - 32;
finishede_y		= (pos_y + boxHeight - y_buffer) - 8;

letter			= 0;
charCount		= 0;
charCount_f		= 0;
text_speed		= 5;
text_speed_c	= 5;
audio_c			= 0;
page			= 0;
str_len			= -1;
pause			= false;
chosen			= false;
choice			= 0;

creator			= noone;
type			= 0;
text			= -1;
text_NE			= -1;
breakpoints		= -1;
nextline		= 0;
text_col		= c_white;
emotion			= 0;

portrait		= 1;
voice			= 1;
font			= fnt_moderndos;

charSize		= 1;
stringHeight	= 1;

//---------------------Effect variables

t			= 0;
amplitude	= 4;
freq		= 2;
ec			= 0;	//effect c



#endregion
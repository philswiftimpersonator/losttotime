/// @description Renable Entity Movement At All Costs

with(par_entity)
{
	can_move = true;
}

set_camera_follow(obj_player, true);
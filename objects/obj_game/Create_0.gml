/// @description Do Important Set Up Stuff


//Create Display Manager
display_manager = instance_create_depth(0, 0, -100, obj_display);


//Sound System
global.volume = 1;
global.mus_vol = 1;
global.sfx_vol = 1;
global.mus_emitter = audio_emitter_create();
global.sfx_emitter = audio_emitter_create();
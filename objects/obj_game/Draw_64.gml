/// @description Draw Debug Stuff

draw_set_font(fnt_moderndos);

draw_text(16, 16, "Lost To Time: ver-" + string(GM_version));

if(instance_exists(obj_player))
{
	draw_text(16, 32, "HSP:" + string_format(obj_player.hspd[0], 2, 2) + " VSP:" + string_format(obj_player.vspd[0], 2, 2) + " MULTI: x" + string_format(obj_player.spd_multi, 2, 2));
	draw_text(16, 48, "XPOS:" + string_format(obj_player.x, 4, 2) + " YPOS:" + string_format(obj_player.y, 4, 2));
	draw_text(16, 64, "XTILE: " + string_format(obj_player.xpos, 3, 0) + " YTILE: " + string_format(obj_player.ypos, 3, 0));
}

if(instance_exists(obj_camera))
{
	draw_text(16, 80, "CAMX: " + string_format(obj_camera.new_x + 160, 4, 2) + " CAMY: " + string_format(obj_camera.new_y + 90, 4, 2));
}

//draw_rectangle(0, 0, 640, 360, false);
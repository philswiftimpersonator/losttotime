/// @description Follow Object Around

if(instance_exists(following))
{
	if(smooth_follow)
	{	
		new_x = lerp(new_x, following.x - cam_w / 2, 0.1);
		new_y = lerp(new_y, following.y - cam_h / 2, 0.1);
	} else {
		new_x = following.x - cam_w / 2;
		new_y = following.y - cam_h / 2;
	}
	
	
	new_x = clamp(new_x, 0, room_width - 320);
	new_y = clamp(new_y, 0, room_height - 180);
	
	var shake_x = random_range(-shake[0], shake[0]),
		shake_y = random_range(-shake[1], shake[1]);
		
	new_x += shake_x;
	new_y += shake_y;
	
	camera_set_view_pos(view_camera[0], new_x, new_y);
	
}

shake[0] *= 0.75;
shake[1] *= 0.75;
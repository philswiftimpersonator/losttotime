/// @description Trigger

if(evnt_inst != noone)
{
	if(!instance_exists(obj_event))
	{
		instance_destroy();
	}
	
	exit;
}

//Check the player exists first
if(instance_exists(obj_player))
{
	//Make Sure To Actually Be In the Same Tile
	if(obj_player.xpos == xpos && obj_player.ypos == ypos)
	{
		//If We're Actually Touching the Trigger
		if(place_meeting(x, y, obj_player))
		{
			//Check if there's been an event set
			if(event_to_trigger == -1)
			{
				print("No event to call, aborting call...");
				exit;
			} else {
				//Call The Event
				evnt_inst = call_event(event_to_trigger);
			}
		}
	}
}
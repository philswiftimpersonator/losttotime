/// @description Warp To Room

//Have We Warped to The Room Yet?
if(!warped)
{
	//Fade in The Rectangle
	fade_alpha = min(fade_alpha+spd, 1);
	
	//If The Rectangle Is Fully Transparent
	if(fade_alpha == 1)
	{
		//Check that the Timer Has Depleted Before Sending Us To The Specified Room
		if(timer == 0)
		{
			//We've Warped
			warped = true;
			
			//Actually Send User to the Next Room
			room_goto(warp_to);
			
			//Set Player Values
			if(instance_exists(obj_player))
			{
				//Position Values
				obj_player.x = new_pos[0];
				obj_player.y = new_pos[1];
				
				//Face Direction
				if(new_face != -1) obj_player.dir = new_face * 90;
			}
		} else { //If Not Depleted, Decrement the Value
			timer = max(0, timer - 1);
		}
	}
} else {
	
	//Fade Out Rectangle
	fade_alpha = max(0, fade_alpha-spd);
	
	//Destroy Ourselves When The Rectangle Is Gone
	if(fade_alpha == 0)
	{
		instance_destroy();
	}
	
}

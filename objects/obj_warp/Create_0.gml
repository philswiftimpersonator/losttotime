/// @description Setup

warp_to = rm_test;
current_room = room;

//Frames We Should Wait Before Changing Rooms
timer = 30;

//Player Values
new_pos = [0, 0];
new_face = -1;

//Used for Opacity of Rectangle, 0 to 1
fade_alpha = 0;

//How Fast The Rectangle Should Appear and Disappear
spd = 0.15;

warped = false;

fade_color = c_black;
//-----------Customise (FOR USER)
playerobject = obj_player;
interact_key = ord("Z");
detection_radius = 32;

myVoice			= sfx_text_blip;
myPortrait		= -1;
myFont			= fnt_moderndos;
myName			= "None";
myStyle         = 2;

myPortraitTalk		= -1;
myPortraitTalk_x	= -1;
myPortraitTalk_y	= -1;
myPortraitIdle		= -1;
myPortraitIdle_x	= -1;
myPortraitIdle_y	= -1;

//-----------Defaults Setup (LEAVE THIS STUFF)
reset_dialogue_defaults();

//Default Values
event_inherited();
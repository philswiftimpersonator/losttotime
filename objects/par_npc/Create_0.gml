/// @description Setup
event_inherited();

myName = "Old Woman";
myText = ["I'm still an absolute fucking boomer, holy shit.", ["Yep.", "ok boomer", "what lol"], "Okay, zoomer."];
myTypes = [0, 1, 0];
myNextLine = [0, [0, 0, 0], 0]

state = states.idle;

//Sprite Array
sprite = [spr_oldwoman_right, spr_oldwoman_up, spr_oldwoman_left, spr_oldwoman_down];

animate_speed = 0;

//Standard Speed
spd = 1;
spd_multi = 1;

//Movement Variables
sprinting = false;

hspd[1] = 0;
vspd[1] = 0;

//Direction Variables
face = 0;
dir = 0;

xpos = x div TILE_WIDTH;
ypos = y div TILE_HEIGHT;

move = false;
textevent = noone;
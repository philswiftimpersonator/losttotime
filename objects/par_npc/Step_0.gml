/// @description Move and Animate NPC
event_inherited();

/*

//Open Textbox If Near Player and Player Presses Interact
if(distance_to_object(obj_player) < 8)
{
	if(keyboard_check_pressed(ord("Z")) && textevent == noone)
	{
		textevent = create_textevent(["I'm a fucking boomer, lmao"], id);
		
		set_camera_follow(id, true);
	} else if(textevent != noone && !instance_exists(textevent)) {
		textevent = noone;
		
		set_camera_follow(obj_player, true);
	}
}
*/

//DEBUG: Face Player

dir = point_direction(x, y, obj_player.x, obj_player.y);


//Face Direction
face = round(dir / 90);
if(face == 4)
{
	face = 0;
}

if(!can_move) 
{
	hspd[0] = 0;
	vspd[0] = 0;
}

//Call Animate Function
animate_entity(face);